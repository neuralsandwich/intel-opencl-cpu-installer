Intel-OpenCL-CPU-Installer
==========================

This script is designed to install the Intel OpenCL CPU implementation on
Ubuntu 14.04.

To use it simply run the following command:

    bash ./install.sh

Contributurs
------------

* Sean Jones <neuralsandwich@gmail.com>
* Ruyman Reyes <ruyman@codeplay.com>
